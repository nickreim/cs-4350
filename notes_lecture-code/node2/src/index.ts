interface ICustomer {
    id: string;
    name: string;
    username: string;
    email: string;
    address: string;
    phone: string;
    website: string;
    company: string;
    avatar: string;
}

export class Main {

    constructor() {
        const button = document.getElementById("populate") as HTMLButtonElement;
        button.onclick = () => this.populateData();
        // button.addEventListener("click", function() => {
        //     this.populateData();
        // }.bind(this));
    }

    public async populateData() {
        let tableBody = document.getElementById("table-body");
        let url = "https://cs4350.herokuapp.com/demo/all";
        let customerData = await this.loadData(url);
        tableBody.innerHTML = this.createHtmlFromArray(customerData);
        console.log("populate data");
    }
    
    public async loadData(url: string): Promise<ICustomer[]> {
        const Response = await fetch(url);
        const json = await Response.json();
        return json as ICustomer[];
        // return fetch(url).then(Response => {
        //     return Response.json();
        // })
    }

    public createHtmlFromArray(dataArray: ICustomer[]): string {
        let listHtml = "";
        let template = document.getElementById("table-template");
        let templateHtml = template.innerHTML;

        for (let i = 0; i < dataArray.length; i++) {
            listHtml += templateHtml
                .replace(/{{id}}/g, dataArray[i].id)
                .replace(/{{name}}/g, dataArray[i].name)
                .replace(/{{username}}/g, dataArray[i].username)
                .replace(/{{email}}/g, dataArray[i].email)
                .replace(/{{address}}/g, dataArray[i].address)
                .replace(/{{phone}}/g, dataArray[i].phone)
                .replace(/{{website}}/g, dataArray[i].website)
                .replace(/{{company}}/g, dataArray[i].company)
                .replace(/{{avatar}}/g, dataArray[i].avatar)
        }
        return listHtml;
    }

}

new Main();