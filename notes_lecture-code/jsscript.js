"use strict";

(function() {
    // var vs let vs const
    let x = 1;

    if (x === 1) {
        let x = 2;
        console.log("inside scope");
        console.log(`x = ${x}`);
    }
    console.log("outside scope");
    console.log(`x = ${x}`);

    var y = 1;
    if (y === 1) {
        var y = 2;
        console.log("inside scope");
        console.log(`y = ${y}`);
    }
    console.log("outside scope");
    console.log(`y = ${y}`);

    const Z = 0;
    console.log(Z);
    
    try {
        Z = 2;
    } catch (error) {
        console.error(error);
    }

    //Truthy & Falsy

    console.log("false == 'false'", false == 'false');
    console.log("false === 'false'", false === 'false');

    console.log("false == '0'", false == '0');
    console.log("false === '0'", false === '0');

    console.log("' \\t\\r\\n ' == 0", ' \t\r\n' == 0);
    console.log("' \\t\\r\\n ' === 0", ' \t\r\n ' === 0);
})();