import { Component, HostListener, OnInit } from '@angular/core';

interface PageData {
  name: string;
  bodyText: string;
  image: string;
  imgHeight: string;
}
interface Content {
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Portfolio';
  
  public bodyImage;

  private data: PageData[];

  constructor() {
  }

  async ngOnInit() {
    await this.grabData();
    this.loadPage(this.data[0]);
  }

  @HostListener('document:click', ['$event'])
  events(event: MouseEvent) {
    const techUsedButtonOn = document.getElementById('techUsedButton');
    techUsedButtonOn.addEventListener('click', event => {
      document.getElementById("techUsed").style.visibility = "visible";
        console.log("here");
    });

    let imageFloat = document.getElementById('bodyImage').style.cssFloat;

    const homeButtons = document.getElementsByClassName('home');
    for (let i = 0; i < homeButtons.length; i++) {
      homeButtons.item(i).addEventListener('click', event => {
        this.loadPage(this.data[0]);
        imageFloat = "right";
      });
    }

    const projectsButton = document.getElementById('projects');
    projectsButton.addEventListener('click', event => {
      this.loadPage(this.data[1]);
      imageFloat = "left";
    });

    const aboutMeButton = document.getElementById('about');
    aboutMeButton.addEventListener('click', event => {
      this.loadPage(this.data[2]);
      imageFloat = "left";
    })

    const contactButton = document.getElementById('contact');
    contactButton.addEventListener('click', event => {
      this.loadPage(this.data[3]);
    })
  }

  public async loadPageData(url: string): Promise<PageData[]> {
    const Response = await fetch(url);
    const json = await Response.json();
    return json as PageData[];
  }

  public async loadArticleData(url: string): Promise<Content> {
    const Response = await fetch(url);
    const json = await Response.json();
    return json as Content;
  }

  public async loadPage(data: PageData) {
    const article = document.getElementById('bodyText') as HTMLElement;
    let content = await this.loadArticleData(data.bodyText);
    article.innerHTML = content.text;
    this.bodyImage = data.image;
    const image = document.getElementById('bodyImage');
    image.style.height = data.imgHeight.toString();
  }

  async grabData() {
    this.data = await this.loadPageData("assets/content.json");
  }

}