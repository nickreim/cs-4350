import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-tech-used',
  templateUrl: './tech-used.component.html',
  styleUrls: ['./tech-used.component.css']
})
export class TechUsedComponent implements OnInit {

  @HostListener('document:click', ['$event'])
  close(e: MouseEvent) {
    const button = document.getElementById("techUsedCloseButton");
    button.addEventListener('click', event => {
      document.getElementById("techUsed").style.visibility = "hidden";
    });
  }

  ngOnInit() {

  }
}
